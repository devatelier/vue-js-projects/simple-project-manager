// Vuetify plugin
// the following might be used the future ...
//import Vue from 'vue'
//import Vuetify from 'vuetify/lib'

import '@mdi/font/css/materialdesignicons.css' // Ensure you are using css-loader
import { createVuetify } from 'vuetify'
import 'vuetify/styles'

//import * as components from 'vuetify/components'
//import * as directives from 'vuetify/directives'

export default createVuetify({
  //components,
  //directives,
  // ssr: true, # enable, if you are using SSR
  icons: {
    defaultSet: 'mdi', // This is already the default value - only for display purposes
  } as any, // please fix later by adding the right type
})


// Vue.use(Vuetify, {iconfont: 'md'})