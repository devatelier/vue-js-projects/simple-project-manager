# simple-project-manager

This is a simple project manager based on the code from The Net Jinja. The code had to be adjusted, since some of the infos of the tutorial were outdated.

see 

https://www.youtube.com/watch?v=2uZYKcKHgU0&list=PL4cUxeGkcC9g0MQZfHwKcuB0Yswgb3gA5&index=1

## Installation 

    cd imageapp
    npm install

    # development webserver (you do not need this, if you are developing with vscode and )
    npm run dev

## steps to use vuetify in a vue.js application as plugin

(vue-cli is currently not supported, so a manual procedure has been used)

The vue skeleton had originally been created with :

    npm init vue@latest

    # vuetify was added by:

    npm add vuetify@3.0.7

    # material font icons
    npm install @mdi/font -D

    # currently v-date-picker is still under construction for vuetify, so we use an alternative
    # see https://vue3datepicker.com/
    
    npm install @vuepic/vue-datepicker


In the plugin folder (to be created) the vuetify.ts plugin was added (see there).

main.ts was adjusted to import the required vuetify plugins

## REST development JSON-server installation:

We are using the very simple json server:

https://github.com/typicode/json-server

it requires node.js > 12.0 and installs with:  

    npm install -g json-server

then create an empty JSON file todo_app.json containing only:

{ "todo_items" : []
}


start the server with:

    json-server --watch todo_app.json -H <your ip, like 192.168.X.x>


Installation was done on a Raspberry Pi and worked very well.


## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).

## Type Support for `.vue` Imports in TS

TypeScript cannot handle type information for `.vue` imports by default, so we replace the `tsc` CLI with `vue-tsc` for type checking. In editors, we need [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin) to make the TypeScript language service aware of `.vue` types.

If the standalone TypeScript plugin doesn't feel fast enough to you, Volar has also implemented a [Take Over Mode](https://github.com/johnsoncodehk/volar/discussions/471#discussioncomment-1361669) that is more performant. You can enable it by the following steps:

1. Disable the built-in TypeScript Extension
    1) Run `Extensions: Show Built-in Extensions` from VSCode's command palette
    2) Find `TypeScript and JavaScript Language Features`, right click and select `Disable (Workspace)`
2. Reload the VSCode window by running `Developer: Reload Window` from the command palette.

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```
